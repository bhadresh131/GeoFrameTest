import XCTest
@testable import GeoPackage

final class GeoPackageTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(GeoPackage().text, "Hello, World!")
    }
}
